package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SuperStructure;

public class ArmExtension extends CommandBase {
    private SuperStructure ss;

    private String state;
    private int extension;


    public ArmExtension(SuperStructure ss2) {
        ss = ss2;
        addRequirements(ss);
    }

    public void initialize() {
    }

    public void extend(String state) {
        switch (state) {
            case "Retract":
                extension = 0;
            case "Intake":
                extension = 2;
            case "Mid":
                extension = 5;
            case "High":
                extension = 10;
        }

        ss.setArm(extension);
    }

    public boolean isFinished() {
        //deadband of 2%??? idk edit this part later
        return (ss.getArmLength()>=0.98*extension && ss.getArmLength()<=1.02*extension);
    }

    
}


