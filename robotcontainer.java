package frc.robot;

import java.io.IOException;

import com.pathplanner.lib.PathPlanner;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import frc.robot.commands.Autonomous.AutonomousCommand;
import frc.robot.commands.Autonomous.EmptyAuto;
import frc.robot.commands.Autonomous.MidOneObj;
import frc.robot.commands.Autonomous.OneObject;
import frc.robot.commands.Autonomous.OneObjectMove;
import frc.robot.commands.Autonomous.TwoObjectNoBump;
import frc.robot.commands.CompoundCommands.ScoreObject;
import frc.robot.commands.Intake.Intake;
import frc.robot.commands.Intake.Outtake;
import frc.robot.Constants.AutoConst;
import frc.robot.commands.Balance;
import frc.robot.commands.DriveCMD;
import frc.robot.commands.ScoreHigh;
import frc.robot.commands.VisionMoveToPosition;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.LED;
import frc.robot.subsystems.Superstructure;
import frc.robot.subsystems.Vision;
import frc.robot.subsystems.Superstructure.GameObject;
import frc.robot.subsystems.Superstructure.MoveState;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.POVButton;
import edu.wpi.first.wpilibj2.command.button.Trigger;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in
 * the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of
 * the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {

    // Controllers
    private final XboxController driverController;
    
    // Subsystems
    private final Drivetrain drivetrain;
    //private final RobotMusic robotMusic;
    final Superstructure superstructure;
    private final Vision vision; 
    private final LED led;

    // Commands
    //private final PlayMusic playMusic;
    private final DriveCMD driveCMD;
    private final Intake intake;
    private final Outtake outtake;
    private final Balance balance;
    private final VisionMoveToPosition visionMoveToPosition;

    // Autonomous Selctor
    private final SendableChooser<AutonomousCommand> autonomousSelector;

    // Current Game Object True=Cone, False=Cube
    public boolean g;
    Field2d field = new Field2d();

    /**
     * The container for the robot. Contains subsystems, OI devices, and commands.
     * @throws IOException
     */

    public RobotContainer() throws IOException {
        // Controllers
        driverController = new XboxController(RobotMap.DRIVE_CONTROLLER);

        // Subsystems
        drivetrain = new Drivetrain();
        //robotMusic = new RobotMusic(drivetrain);
        led = new LED();
        superstructure = new Superstructure(led);
        vision = new Vision();

        // Commands
        //playMusic = new PlayMusic(robotMusic);
        driveCMD = new DriveCMD(
            drivetrain,
            () -> driverController.getLeftY(),
            () -> driverController.getLeftX(),
            () -> driverController.getRightX()
        );
        intake = new Intake(superstructure, () -> g);
        outtake = new Outtake(superstructure, () -> g);
        balance = new Balance(drivetrain);
        visionMoveToPosition = new VisionMoveToPosition(drivetrain, vision, () -> g?GameObject.CONE:GameObject.CUBE, false);
        g = true;

        // Be mindful of this. Resets superstructure to a predetermined state in Constants.java:
        superstructure.resetState();

        // Autonomous Selector
        autonomousSelector = new SendableChooser<>();

        configButtonBindings();
    }

    private void configButtonBindings() {
        // Driving Buttons
        drivetrain.setDefaultCommand(driveCMD);
        new Trigger(() -> driverController.getRawButton(8)).onTrue(new InstantCommand(() -> {drivetrain.resetGyro(); drivetrain.setGyroOffset(0);}, drivetrain));
        new Trigger(() -> driverController.getLeftTriggerAxis() > 0.8).onTrue(new InstantCommand(() -> driveCMD.speedToggle()));
        new Trigger(() -> driverController.getRawButton(7)).onTrue(balance).onFalse(new InstantCommand(() -> balance.cancel()));

        // Music Buttons | Will not activate in a competition match
        /*if (!DriverStation.isFMSAttached()) {
            new JoystickButton(driverController, PS4.L_BUTTON).toggleOnTrue(playMusic);
            new JoystickButton(driverController, PS4.L_MENU).onTrue(new InstantCommand(() -> playMusic.nextSong()));
        }*/

        // Wrist Adjustment
        new POVButton(driverController, 90).onTrue(new InstantCommand(() -> superstructure.incrementWOffset(7)));
        new POVButton(driverController, 270).onTrue(new InstantCommand(() -> superstructure.incrementWOffset(-7)));

        // Arm Position Buttons
        new Trigger(() -> driverController.getYButton()).onTrue(new ScoreHigh("High"));
        /*new Trigger(() -> driverController.getBButton()).onTrue(new ScoreObject(MoveState.SCORE_MIDDLE, superstructure));
        new Trigger(() -> driverController.getAButton()).onTrue(new ScoreObject(MoveState.SCORE_LOW, superstructure));
        new Trigger(() -> driverController.getXButton()).onTrue(new ScoreObject(MoveState.STOW, superstructure));*/
        
        // Intake Buttons
        new Trigger(() -> driverController.getRightTriggerAxis() > 0.8).toggleOnTrue(intake);
        new Trigger(() -> driverController.getRightBumper()).onTrue(outtake);

        // LEDs + Game Object Toggle
        new Trigger(() -> driverController.getLeftBumper()).onTrue(new InstantCommand(() -> {g = !g; led.setColor(g?Color.kOrangeRed:Color.kPurple);}));
        
        
        // Vision Target
        new POVButton(driverController, 0).whileTrue(visionMoveToPosition);
        new POVButton(driverController, 180).onTrue(new ScoreObject(MoveState.PICKUP, superstructure));
    }

    public void loadTrajectories() {
        // Configure Autonomous Selector    
        autonomousSelector.setDefaultOption("No Autonomous", new EmptyAuto(drivetrain));
        //autonomousSelector.addOption("Example", new ExampleAuto(PathPlanner.loadPathGroup("Example", AutoConst.autoConstraints), drivetrain));
        autonomousSelector.addOption("Two Object No Bump", new TwoObjectNoBump(PathPlanner.loadPathGroup("2objramp", AutoConst.autoConstraints), drivetrain, superstructure));
        autonomousSelector.addOption("One Object Ramp", new MidOneObj(PathPlanner.loadPathGroup("1objMidRamp", AutoConst.autoConstraints), drivetrain, superstructure, () -> led.setColor(Color.kPurple)));
        autonomousSelector.addOption("Side One Object Move", new OneObjectMove(PathPlanner.loadPathGroup("null", AutoConst.autoConstraints), drivetrain, superstructure));
        autonomousSelector.addOption("One Object Stationary", new OneObject(PathPlanner.loadPathGroup("null", AutoConst.autoConstraints), drivetrain, superstructure));
        SmartDashboard.putData(autonomousSelector);
    }

    public void updateField() {
        field.setRobotPose(drivetrain.getPose2d());
    }

    public void resetGyro() {
        drivetrain.resetGyro();
    }

    public void disable() {
        superstructure.stop();
        drivetrain.stopModules();
    }

    public void setLEDSignal(boolean s) {
        led.setSignaling(s);
    }

    public void setLEDColor(Color c) {
        led.setColor(c);
    }

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() {
        AutonomousCommand autocmd = autonomousSelector.getSelected();
        autocmd.resetOdometry();
        return autocmd;
    }

    public void periodic() {
        SmartDashboard.putBoolean("Cone?", g);
        SmartDashboard.putBoolean("Cube?", !g);
    }
}

