package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SuperStructure;

public class ArmRotation extends CommandBase {
    private SuperStructure ss;

    private String state;
    private double theta;


    public ArmRotation(SuperStructure ss2) {
        ss = ss2;
        addRequirements(ss);
    }



    public void rotate(String state) {
        switch (state) {
            case "Retract":
                theta = 0;
            case "Intake":
                theta = 2;
            case "Mid":
                theta = 4;
            case "High":
                theta = 10;
        }

        ss.spinArm(theta);
    }

    public boolean isFinished() {
        //deadband of 2%??? idk edit this part later
        return (ss.getSpin()>=0.98*theta && ss.getSpin()<=1.02*theta);
    }

    
}


