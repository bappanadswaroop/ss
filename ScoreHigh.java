package frc.robot.commands;


import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SuperStructure;

public class ScoreHigh extends CommandBase {
    private ArmRotation alpha;
    private ArmExtension L;

    private String state;


    public ScoreHigh(String input) {
        state = input;
    }

    public void initialize(){}
    public void execute() {
        alpha.rotate(state);
        L.extend(state);
    }

    public boolean isFinished() {
        return (alpha.isFinished()&&L.isFinished());
    }
}
