package frc.robot.commands;

import edu.wpi.first.math.controller.PIDController;
import frc.robot.subsystems.SuperStructure;

public class WristRotation {
    private SuperStructure ss;

    private double theta;

    public WristRotation(SuperStructure sup) {
        ss = sup;
 
    }

    public void rotate(String state) {
        switch (state) {
            case "Stow":
                theta = 0.0;
            case "Intake":
                theta = 90;
            case "Low":
                theta = 30; 
            case "Mid":
                theta = 15.0;
            case "High":
                theta = 60;
        }
    }
}

