package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.TalonFX;


import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.RobotMap.Hardware;

public class SuperStructure extends SubsystemBase {
    TalonFX armR,armL, telescoping;
    CANSparkMax wrist;
    PIDController armPID, armPIDR;

    public SuperStructure() {
        armR = new TalonFX(Hardware.ARMLEFT);
        armL = new TalonFX(Hardware.ARMRIGHT);

        armPID = new PIDController(Constants.SuperStructureConstants.armkP, Constants.SuperStructureConstants.armkI, Constants.SuperStructureConstants.armkD);
        armPIDR = new PIDController(Constants.SuperStructureConstants.armkPr, Constants.SuperStructureConstants.armkIr, Constants.SuperStructureConstants.armkDr);

        telescoping = new TalonFX(Hardware.TELESCOPING);

        armL.follow(armR);
        armL.setInverted(InvertType.FollowMaster);
    }
    
    public void setArm(double extension) {
        armPID.setSetpoint(extension);
        telescoping.set(ControlMode.Position, armPID.calculate(telescoping.getSelectedSensorPosition(),extension));
    }

    public void spinArm(double theta) {
        armPIDR.setSetpoint(theta);
        armR.set(ControlMode.Position, armPIDR.calculate(armR.getSelectedSensorPosition(), theta));
    }

    public double getArmLength(){
        return telescoping.getSelectedSensorPosition();
    }

    public double getSpin() {
        return armR.getSelectedSensorPosition();
    }
}

